﻿

let filter (f : 'a -> bool) (items : 'a list)  =

    let rec filterInternal input result =

        match input with
        | [] ->
            result

        | head :: tail ->
            if (f head) then
                filterInternal tail (head :: result)
            else
                filterInternal tail result

    filterInternal items []
    |> List.rev


[<EntryPoint>]
let main argv = 
    
    let printList (items : 'a list) = 
        items |> List.iter (fun i -> printf "Item = %A\n" i)

    let l1 = [1;2;3;4;5;6]

    let l2 = l1 |> filter (fun i -> (i % 2) = 0)

    
    printList l1
    printf "-----------\n"
    printList l2

    0 // return an integer exit code
