#include "stdafx.h"


struct MyResumable
{
	struct Promise
	{
		int value;

		MyResumable get_return_object()
		{
			return MyResumable {std::experimental::coroutine_handle<Promise>::from_promise(*this)};
		}

		std::experimental::suspend_never initial_suspend()
		{
			return {};
		}

		std::experimental::suspend_always final_suspend()
		{
			return {};
		}

		/*void return_void()
		{
		}*/

		void return_value(int v)
		{
			std::cout << "ret" << std::endl;
			value = v;
		}
	};

	typedef Promise promise_type;

	std::experimental::coroutine_handle<Promise> coro = nullptr;

	MyResumable(std::experimental::coroutine_handle<Promise> coro_)
		: coro(coro_)
	{}


	MyResumable(MyResumable&& other)
		: coro(other.coro)
	{
		other.coro = nullptr;
	}

	~MyResumable()
	{
		if (coro)
		{
			coro.destroy();
		}
	}

	MyResumable& operator = (MyResumable&& other)
	{
		coro = other.coro;
		other.coro = nullptr;

		return *this;
	}

	void resume() const
	{
		coro.resume();
	}

	bool await_ready() const noexcept
	{
		return true;
	}

	void await_suspend(std::experimental::coroutine_handle<Promise> )
	{
		std::cout << "Await suspend" << std::endl;
	}

	void await_resume() noexcept
	{}

	
	int get()
	{
		return coro.promise().value;
	}

	//void await_suspend(
};

MyResumable counter(std::string some)
{
	std::cout << "counter(" << some << ") was called\n";
	for (unsigned i = 1; ; ++i)
	{
		co_await std::experimental::suspend_always{};
		std::cout << "counter: resumed (#" << i << "):" << some.c_str() << "\n";
	}

	co_return 75;
}

MyResumable get_value()
{
	std::cout << "get_value: called\n";
	
	//co_await std::experimental::suspend_always{};
	
	std::cout << "get_value: resumed\n";
	
	co_return 30;
}


std::future<int> xl()
{
	co_return 75;

}



int main()
{
	auto aa = get_value();

	//aa.resume();*/

	auto xx = xl();


	std::cout << aa.get() << std::endl;
	std::cout << xx.get() << std::endl;

	/*auto aa = counter("AA");
	auto bb = counter("BB");

	aa.resume();
	bb.resume();

	aa.resume();
	bb.resume();
*/

	return 0;
}

