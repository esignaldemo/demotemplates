﻿

let rec forEach f (items : 'a list) =

    match items with
    | [] ->
        ()

    | head :: tail ->
        f (head)
        forEach f tail

[<EntryPoint>]
let main argv = 
    
    let l1 = [1;2;3;4;5]

    forEach (fun i -> printf "item = %A\n" i) l1

    0
