#include "stdafx.h"

#include <iostream>
#include <tuple>
#include <type_traits>


template<typename ...>
struct TypeList
{};


template<typename TL>
struct ForEach;


template<>
struct ForEach<TypeList<>>
{
	static void f()
	{}
};


template<typename Head, typename ... Tail>
struct ForEach<TypeList<Head, Tail...>>
{
	static void f()
	{
		std::cout << typeid(Head).name() << std::endl;

		ForEach<TypeList<Tail...>>::f();
	}
};



int main()
{
	typedef TypeList<int, float, double> l1;

	ForEach<l1>::f();

	return 0;
}

