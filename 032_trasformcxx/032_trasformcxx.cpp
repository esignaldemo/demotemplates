#include "stdafx.h"

#include <iostream>
#include <tuple>
#include <type_traits>


template<typename ...>
struct TypeList
{};


template<typename T>
struct Wrapper
{
};


template<typename T>
struct MakeWrapper
{
	typedef Wrapper<T> type;
};


template<template <typename T> class F, typename InputTL, typename OutputTL = TypeList<>>
struct Transform;


template<template <typename T> class F, typename OutputTL>
struct Transform<F, TypeList<>, OutputTL>
{
	typedef OutputTL type;
};


template<template <typename T> class F, typename InHead, typename ... InTail, typename ... OutputItems>
struct Transform<F, TypeList<InHead, InTail...>, TypeList<OutputItems...>>
{
	using El = typename F<InHead>::type;

	typedef typename Transform<F, TypeList<InTail...>, TypeList<El, OutputItems...>>::type type;
};


int main()
{
	typedef TypeList<int, float, double> l1;

	typedef Transform<MakeWrapper, l1, TypeList<>>::type l2;

	std::cout << typeid(l1).name() << std::endl;
	std::cout << typeid(l2).name() << std::endl;

	return 0;
}

