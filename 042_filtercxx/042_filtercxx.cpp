// 042_filtercxx.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <tuple>
#include <type_traits>


template<typename ...>
struct TypeList
{};


template<template <typename> class F, typename TL, typename Result = TypeList<>>
struct Filter ;


template<template <typename> class F, typename Result>
struct Filter<F, TypeList<>, Result>
{
	typedef Result type;
};


template<template <typename> class F, typename Head, typename ... Tail, typename ... Result>
struct Filter<F, TypeList<Head, Tail...>, TypeList<Result...>>
{
	typedef std::conditional_t<F<Head>::value,
		typename Filter<F, TypeList<Tail...>, TypeList<Result..., Head>>::type,
		typename Filter<F, TypeList<Tail...>, TypeList<Result...>>::type> type;
};


int main()
{
	typedef TypeList<int, float, short, unsigned, double, long, TypeList<>> l1;

	typedef Filter<std::is_integral, l1>::type l2;

	std::cout << typeid(l1).name() << std::endl;
	std::cout << typeid(l2).name() << std::endl;

	return 0;
}

