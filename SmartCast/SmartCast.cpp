// SmartCast.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <type_traits>

//-----------------------------------------------------------------------------
template<typename U, typename T>
U smartCast(T& instance)
{
	return static_cast<U>(instance);
}


template<typename U, typename T, std::enable_if_t<std::is_convertible_v<T*, U>>* = nullptr>
auto smartCast(T* instance)
{
	static_assert(std::is_pointer_v<U>, "Type must be pointer");

	return static_cast<U>(instance);
}


template<typename U, typename T, std::enable_if_t<!std::is_convertible_v<T*, U>>* = nullptr>
auto smartCast(T* instance)
{
	static_assert(std::is_pointer_v<U>, "Type must be pointer");

	return dynamic_cast<U>(instance);
}


//template<typename U, typename T>
//U smartCast(T* instance)
//{
//	return static_cast<U>(instance);
//}



//-----------------------------------------------------------------------------
struct IBase
{
	virtual void base() const = 0;
};


struct ILogger
{
	virtual void print() const = 0;
};


class Component : public IBase
{
	virtual void base() const override
	{}

};





int main()
{
	Component instance;
	
	auto base = smartCast<IBase*>(&instance);

	ILogger* logger = smartCast<ILogger*>(&instance);


	//ILogger& logger = smartCast<ILogger&>(instance);




	return 0;
}

