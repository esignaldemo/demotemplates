﻿
let transform (f : 'a -> 'b) (items : 'a list) =

    let rec transformInternal (input : 'a list) (output : 'b list) =
        
        match input with
        | [] ->
            output

        | head :: tail ->
            let el = f head

            transformInternal tail (el :: output)

    List.rev(transformInternal items [])


[<EntryPoint>]
let main argv = 
    
    let printList (items : 'a list) = 
        
        items |> List.iter (fun i -> printf "Item = %A\n" i)


    let l1 = [1;2;3;4;5;6]

    printList l1

    let l2 = transform (fun i -> (sprintf "X x 10 = [%A]" (i * 10)) ) l1

    printList l2


    0 // return an integer exit code
